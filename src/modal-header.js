import {bindable} from 'aurelia-framework';

@bindable({name: 'title', defaultValue: ''})
export class ModalHeader { }
