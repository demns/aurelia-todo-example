export default class Todo {
  constructor(message, checked = false) {
    this.message = message;
    this.checked = checked;
  }
}
