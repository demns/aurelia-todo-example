import Todo from './todo';

export class App {
  constructor() {
    this.name = 'TodoList';
    this.todos = [];
    this.url = '1231';
  }

  addTodo() {
    this.todos.push(new Todo(this.description));
  }

  removeTodo(todo) {
    const index = this.todos.indexOf(todo);
    if (index !== -1) {
      this.todos.splice(index, 1);
    }
  }
}
